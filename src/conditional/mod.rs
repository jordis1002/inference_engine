use crate::atom::*;
use crate::parser::*;
use eval::eval;
use pest::*;
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;

mod tests;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Deserialize, Serialize)]
pub struct Conditional {
    guard: String,
    pub result: Atom,
}

impl Conditional {
    pub fn atoms(&self) -> Vec<Atom> {
        let string_is_atom = |string: &str| string.starts_with('[') && string.ends_with(']');

        self.guard
            .split_whitespace()
            .filter(|s| string_is_atom(s))
            .map(|s| {
                let text = s.trim_matches('[').trim_matches(']');
                Atom::new(text)
            })
            .collect()
    }

    fn new_from_string<S: Into<String>>(parse_string: S) -> Self {
        let parse_string = parse_string.into();
        let mut inner_rules = LMSParser::parse(Rule::conditional, &parse_string)
            .expect("unsuccessful parse")
            .next()
            .unwrap()
            .into_inner();
        let guard = inner_rules.next().unwrap().as_str();
        let result = inner_rules.next().unwrap().into_inner().as_str();
        Conditional::new(guard, result)
    }

    pub fn new<S: Into<String>>(guard: S, atom_text: S) -> Self {
        let guard = guard.into();
        let text = atom_text.into();
        let result = Atom::new(text);
        Conditional { guard, result }
    }

    fn guard_conversion(&self, context: &HashMap<Atom, AtomState>) -> String {
        let guard: String = self
            .guard
            .split_whitespace()
            .map(|s| {
                let mut s = if s.starts_with('[') && s.ends_with(']') {
                    let mut s = s.to_string();
                    s.retain(|c| c != '[' && c != ']');
                    let state = context.get(&Atom::new(s));
                    if let Some(state) = state {
                        match state {
                            eval::Value::Bool(bool) => bool.to_string(),
                            eval::Value::Number(number) => number.to_string(),
                            eval::Value::String(string) => format!("{:?}", string),
                            _ => panic!("Unsupported value"),
                        }
                    } else {
                        "null".to_string()
                    }
                } else {
                    s.to_string()
                };
                s.push(' ');
                s
            })
            .collect();
        guard.trim().to_string()
    }

    pub fn evaluate(&self, context: &HashMap<Atom, AtomState>) -> (Atom, Option<AtomState>) {
        let evaluation = {
            if self
                .guard_conversion(&context)
                .split_whitespace()
                .any(|string| string == "null")
            {
                None
            } else {
                match eval(&self.guard_conversion(&context)) {
                    Ok(value) => {
                        if value == eval::Value::Null {
                            None
                        } else {
                            Some(value)
                        }
                    }
                    Err(_) => None,
                }
            }
        };

        (self.result.clone(), evaluation)
    }
}
