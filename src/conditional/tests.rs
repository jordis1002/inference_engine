use super::*;

#[test]
fn test_from_string() {
    let parse_string = "if ([youAre]) then [whatAmI] end";
    let result = Conditional::new_from_string(parse_string);
    let expected = Conditional {
        guard: "[youAre]".to_string(),
        result: Atom::new("whatAmI"),
    };
    assert_eq!(result, expected);
}

#[test]
fn test_guard_conversion_1() {
    let conditional = Conditional::new("[toBe] || ( ! [toBe] )", "thatIsTheQuestion");
    let mut context: HashMap<Atom, AtomState> = HashMap::new();
    context.insert(Atom::new("toBe"), eval::Value::Bool(true));

    let result = conditional.guard_conversion(&context);
    let expected = "true || ( ! true )";
    assert_eq!(result, expected);
}

#[test]
fn test_conditional_always_true_1() {
    let conditional = Conditional::new("[toBe] || ( ! [toBe] )", "thatIsTheQuestion");

    let mut context: HashMap<Atom, AtomState> = HashMap::new();
    context.insert(Atom::new("toBe"), eval::Value::Bool(true));
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, Some(eval::Value::Bool(true)));
    assert_eq!(result, expected);
}

#[test]
fn test_conditional_always_true_2() {
    let conditional = Conditional::new("[toBe] || ( ! [toBe] )", "thatIsTheQuestion");

    let mut context: HashMap<Atom, AtomState> = HashMap::new();
    context.insert(Atom::new("toBe"), eval::Value::Bool(false));
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, Some(eval::Value::Bool(true)));
    assert_eq!(result, expected);
}

#[test]
fn test_conditional_equality() {
    use serde_json::json;
    let conditional = Conditional::new("[uno] + [drei] == 4", "thatIsTheQuestion");
    let mut context: HashMap<Atom, AtomState> = HashMap::new();
    context.insert(Atom::new("uno"), json!(1.0));
    context.insert(Atom::new("drei"), json!(3.0));
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, Some(eval::Value::Bool(true)));
    assert_eq!(result, expected);
}

#[test]
fn test_conditional_inequality() {
    use serde_json::json;
    let mut context: HashMap<Atom, AtomState> = HashMap::new();
    context.insert(Atom::new("uno"), json!(1.0));
    context.insert(Atom::new("drei"), json!(3.0));

    let conditional = Conditional::new("[drei] - [uno] != 3", "thatIsTheQuestion");
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, Some(eval::Value::Bool(true)));
    assert_eq!(result, expected);

    let conditional = Conditional::new("[drei] - [uno] < 3.0", "thatIsTheQuestion");
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, Some(eval::Value::Bool(true)));
    assert_eq!(result, expected);

    let conditional = Conditional::new("( [drei] - [uno] ) > 1", "thatIsTheQuestion");
    println!("{:#?}", conditional.guard_conversion(&context));
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, Some(eval::Value::Bool(true)));
    assert_eq!(result, expected);
}

#[test]
fn test_conditional_always_false_1() {
    let conditional = Conditional::new("[toBe] && ( ! [toBe] )", "thatIsTheQuestion");

    let mut context: HashMap<Atom, AtomState> = HashMap::new();
    context.insert(Atom::new("toBe"), eval::Value::Bool(true));
    println!("{}", conditional.guard_conversion(&context));
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, Some(eval::Value::Bool(false)));
    assert_eq!(result, expected);
}

#[test]
fn test_conditional_always_false_2() {
    let conditional = Conditional::new("[toBe] && ( ! [toBe] )", "thatIsTheQuestion");

    let mut context: HashMap<Atom, AtomState> = HashMap::new();
    context.insert(Atom::new("toBe"), eval::Value::Bool(false));
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, Some(eval::Value::Bool(false)));
    assert_eq!(result, expected);
}

#[test]
fn test_conditional_always_none_1() {
    let conditional = Conditional::new("[toBe] or ( not [toBe] )", "thatIsTheQuestion");

    let context: HashMap<Atom, AtomState> = HashMap::new();
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, None);
    assert_eq!(result, expected);
}

#[test]
fn test_conditional_always_none_2() {
    let conditional = Conditional::new("[toBe] and ( not [toBe] )", "thatIsTheQuestion");

    let context: HashMap<Atom, AtomState> = HashMap::new();
    let result = conditional.evaluate(&context);
    let expected = (conditional.result, None);
    assert_eq!(result, expected);
}
