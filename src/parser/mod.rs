use pest_derive::*;

// This include forces recompiling this source file if the grammar file changes.
// Uncomment it when doing changes to the .pest file
const _GRAMMAR: &str = include_str!("lms.pest");

#[derive(Parser)]
#[grammar = "parser/lms.pest"]
pub struct LMSParser;
