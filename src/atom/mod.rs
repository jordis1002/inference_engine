use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Hash, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct Atom {
    pub text: String,
}

impl Atom {
    pub fn new<S: Into<String>>(text: S) -> Self {
        let text = text.into();
        Atom { text }
    }
}

pub type AtomState = eval::Value;

// impl AtomState {
//     pub fn new(string: &str) -> Self {
//         match string {
//             "Yes" => AtomState::Bool(true),
//             "No" => AtomState::Bool(false),
//             string if string.parse::<f64>().is_ok() => {
//                 AtomState::Numeric(string.parse::<f64>().unwrap())
//             }
//             _ => unreachable!(),
//         }
//     }
// }
