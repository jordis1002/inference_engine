use crate::atom::*;
use crate::conditional::*;
use crate::parser::*;
use indexmap::IndexSet;
use pest::*;
use serde_derive::{Deserialize, Serialize};
use std::collections::HashSet;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct KnowledgeBase {
    pub inferences: IndexSet<Inference>,
    pub conditionals: IndexSet<Conditional>,
    pub questions: IndexSet<Question>,
}

impl KnowledgeBase {
    pub fn new() -> Self {
        Default::default()
    }

    fn insert_question(&mut self, question: Question) {
        self.questions.insert(question);
    }

    fn insert_inference(&mut self, inference: Inference) {
        self.inferences.insert(inference);
    }

    fn insert_conditional(&mut self, conditional: Conditional) {
        self.conditionals.insert(conditional);
    }

    pub fn parse<S: Into<String>>(unparsed: S) -> KnowledgeBase {
        let unparsed = unparsed.into();
        serde_yaml::from_str(&unparsed).expect("unsuccessful_parse")
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct Inference {
    pub atom: Atom,
    pub text: String,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct Question {
    pub atom: Atom,
    pub text: String,
    pub choices: Vec<String>,
    pub description: String,
    pub picture: String,
}
