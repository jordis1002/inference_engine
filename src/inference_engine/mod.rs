use crate::atom::{Atom, AtomState};
use crate::knowledge_base::{Inference, KnowledgeBase, Question};
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;
use std::str::FromStr;

/// An engine that infers from rules contained within a [knowledge base](crate::knowledge_base::KnowledgeBase).
///
/// The system applies logical rules provided in its `KnowledgeBase` to deduce
/// new information. The knowledge base contains the facts, operable atoms, and
/// goal states and the inference engine maintains the current state of the
/// inference.
///
/// See the [`prepare`](crate::InferenceEngine::prepare()) function and the
/// configuration functions on the [builder](crate::InferenceEngineBuilder) for
/// how to construct an inference engine.
#[derive(Debug, Serialize, Deserialize)]
pub struct InferenceEngine {
    knowledge_base: KnowledgeBase,
    inferred: HashMap<Atom, AtomState>,
}

impl InferenceEngine {
    /// Prepare an inference engine by creating a builder.
    ///
    /// The builder allows for convenient chaining of configuration options.
    /// # Examples
    /// The build options can also be chained together into a one-liner.
    /// ```rust
    /// # fn main() {
    /// # if false {
    /// let engine = inference_engine::prepare().with_knowledge_base_file("path_to_my_file");
    /// # }
    /// # }
    /// ```
    ///
    /// For more complex configuration, the builder creation and configuration steps may be separated.
    ///
    /// ```rust
    /// # fn main() {
    /// # if false {
    /// let use_file = true;
    /// let engine_builder = inference_engine::prepare();
    /// let engine = if use_file {
    ///     engine_builder.with_knowledge_base_file("path_to_my_file")
    /// } else {
    ///     engine_builder.with_knowledge_base("knowledge_base_string")
    /// };
    /// # }
    /// # }
    /// ```
    pub fn prepare() -> InferenceEngineBuilder {
        InferenceEngineBuilder {}
    }

    /// Get the next question to ask to progress inference. If no questions can
    /// be asked to further progress the inference, `None` is returned. This may occur in two cases:
    /// 1. No goals remain unevaluated
    /// 2. A goal state resulting in `true` has been reached
    ///
    /// See the function
    /// [`answer_question`](InferenceEngine::answer_question()) for
    /// inputting a user's answer to a question.
    ///
    /// # Examples
    ///
    /// Continue displaying the next question to ask.
    /// ```rust
    /// # fn main() {
    /// # let knowledge_base = "---\ninferences: []\nconditionals: []\nquestions: []";
    /// # let inference_engine = inference_engine::prepare().with_knowledge_base(knowledge_base);
    /// while let Some(question) = inference_engine.next_question() {
    ///     println!("To progress the inference, you should ask the user {:#?}", question);
    /// }
    /// # }
    /// ```
    pub fn next_question(&self) -> Option<Question> {
        // If a goal has already been reached, return None.
        if self.knowledge_base.inferences.iter().any(|goal| {
            self.inferred
                .get(&goal.atom)
                .map_or(false, |state| match state {
                    eval::Value::Bool(val) => *val,
                    val => unreachable!(val),
                })
        }) {
            return None;
        }

        // For every goal we can infer
        for inference_goal in &self.knowledge_base.inferences {
            // If there is some question that progresses that goal
            if let Some(question) = self.backward_chain(&inference_goal.atom) {
                // Return that question
                return Some(question);
            }
        }
        None
    }

    fn backward_chain(&self, atom: &Atom) -> Option<Question> {
        // If that atom has not already been resolved
        if !self.inferred.contains_key(atom) {
            // If some conditional exists which has that atom as a result
            if let Some(conditional) = self
                .knowledge_base
                .conditionals
                .iter()
                .find(|conditional| conditional.result == *atom)
            {
                // Check all the atoms that are required for that conditional
                for atom in conditional.atoms() {
                    // If that atom has not been inferred yet
                    if !self.inferred.contains_key(&atom) {
                        // If there is some question that immediately answers that atom
                        if let Some(question) = self
                            .knowledge_base
                            .questions
                            .iter()
                            .find(|question| question.atom == atom)
                        {
                            // Return that question
                            return Some(question.clone());
                        } else {
                            // Recursively check if there is a question to resolve an element higher in the chain for that atom.
                            return self.backward_chain(&atom);
                        }
                    }
                }
            }
        }
        None
    }

    pub fn reached_goal(&self) -> Option<Inference> {
        for inference in &self.knowledge_base.inferences {
            if let Some(val) = self.inferred.get(&inference.atom) {
                if let eval::Value::Bool(val) = val {
                    if *val {
                        return Some(inference.clone());
                    }
                }
            }
        }
        None
    }

    pub fn add_state(&mut self, atom: Atom, answer: &str) {
        let answer = match answer {
            "Yes" | "true" => eval::Value::from_str("true").unwrap(),
            "No" | "false" => eval::Value::from_str("false").unwrap(),
            string if string.parse::<f64>().is_ok() => eval::Value::from_str(string).unwrap(),
            string => eval::Value::String(string.to_string()),
        };
        self.inferred.insert(atom, answer);
        self.infer();
    }

    /// Store the `answer` for some `question`'s primary atom in the inferred state.
    pub fn answer_question(&mut self, question: &Question, answer: &str) {
        self.add_state(question.atom.clone(), answer);
    }

    // Inference by forward chaining
    // FIXME short-circuit evaluation does not currently work:
    // Consider the evaluation of "false && \"None\" && \"None\""
    fn infer(&mut self) {
        // Continue to infer, until no changes occur
        loop {
            let mut changed = false;

            // For every rule
            for conditional in &self.knowledge_base.conditionals {
                // If that rule does not contribute
                if self.inferred.contains_key(&conditional.result) {
                    continue;
                }

                // If that rule results in a value for some atom
                if let (atom, Some(atom_state)) = conditional.evaluate(&self.inferred) {
                    // Insert that value for that atom in the inferred fact base.
                    self.inferred.insert(atom, atom_state);
                    changed = true;
                }
            }
            if !changed {
                break;
            }
        }
    }
}

pub struct InferenceEngineBuilder;

impl InferenceEngineBuilder {
    /// Creates an inference engine based on a knowledge base provided as a string.

    pub fn with_knowledge_base<S: Into<String>>(&self, string: S) -> InferenceEngine {
        let string = string.into();
        InferenceEngine {
            knowledge_base: KnowledgeBase::parse(string),
            inferred: HashMap::new(),
        }
    }

    /// Creates an inference engine based on a path to a knowledge file.
    pub fn with_knowledge_base_file<S: Into<String>>(&self, path: S) -> InferenceEngine {
        let path = path.into();
        let unparsed =
            fs::read_to_string(&path).unwrap_or_else(|_| panic!("file: {} not found", path));
        self.with_knowledge_base(unparsed)
    }
}
