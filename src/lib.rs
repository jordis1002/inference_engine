#![feature(custom_attribute, uniform_paths)]

//! # Inference Engine - Core API Documentation
//!
//! A library for performing inferences on knowledge bases.
//!
//! ## Usage
//!
//! First, depend on `inference_engine` in `Cargo.toml`:
//! This library is not yet on [crates.io](https://crates.io),
//! so point your dependencies to the [gitlab.com](https://gitlab.com/jordis1002/inference_engine) project.
//!
//! ```toml
//! [dependencies]
//! inference_engine = { git = "https://gitlab.com/jordis1002/inference_engine" }
//! ```
//!
//! If you're using the pre-2018 edition of Rust, add the following to the top of your `main.rs` file:
//!
//! ```rust
//! extern crate inference_engine;
//! ```
//!
//! A simple example of its usage:
//! ```rust
//! fn main() {
//! # if false {
//!     let engine = inference_engine::prepare().with_knowledge_base_file("path_to_my_file");
//!     let question = engine.next_question();
//! # }
//! }
//! ```

pub use crate::atom::Atom;
pub use crate::inference_engine::{InferenceEngine, InferenceEngineBuilder};
pub use crate::knowledge_base::{Inference, KnowledgeBase, Question};

mod atom;
mod conditional;
mod inference_engine;
mod knowledge_base;
mod parser;

/// Alias to [`InferenceEngine::prepare()`] Creates a new instance of `InferenceEngineBuilder`.
pub fn prepare() -> InferenceEngineBuilder {
    InferenceEngine::prepare()
}

#[cfg(test)]
mod tests;
